from django.shortcuts import render, get_object_or_404
from projets.models import Projet, Category


def projet_list(request):
    if request.method == 'GET': 
        projet = Projet.objects.all()
        category = Category.objects.all()
        return render(request, 'projet_list.html', {'projet':projet}, {'category':category})

def one_project(request, pk):
    projet = get_object_or_404(Projet, pk=pk)
    return render(request, 'projet.html', {'projet':projet})

def projet_all(request):
    projet = Projet.objects.all()
    return render(request, 'projet_all.html', {'projet':projet}, {'category': Category.objects.all()})