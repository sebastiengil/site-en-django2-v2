from django.urls import path
from . import views

urlpatterns = [
    path('', views.projet_list, name='projet_list'),
    path('allprojets/', views.projet_all, name='projet_all'),
    path('projet/<int:pk>/', views.one_project, name='projet'),
]